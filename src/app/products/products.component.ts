import { Component, OnInit } from '@angular/core';
import { Producto } from './producto';
import { ProductoService } from './producto.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html'
})
export class ProductsComponent implements OnInit {

  products: Producto[];
  constructor(private productoService: ProductoService) { }

  ngOnInit() {
    this.productoService.getProducts().subscribe(
      products => this.products = products
    );
  }

  delete(producto: Producto): void{ 
    Swal.fire({

        title: 'Está Seguro?',
        text: `Seguro que desea eliminar el producto ${producto.nombre}`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!',
        cancelButtonText: 'No, Cancelar!'

    }).then((result) => {

      if (result.value) {
        this.productoService.delete(producto.id).subscribe(
          response => {
            this.products = this.products.filter(prod => prod !== producto)
            Swal.fire(
              'Eliminado!',
              'resgistro borrado exitosamente',
              'success'
            )
          }
        )
        
      }
    })
      }
}

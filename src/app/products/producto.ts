export class Producto {
  id: number;
  nombre: string;
  precio: number;
  fecha: Date;
  tipoProducto: String;
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Producto } from './producto';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2'

import { Router } from '@angular/router'


@Injectable()
export class ProductoService {

private urlEndPoint: string = 'http://localhost:8080/product/listar';
private urlEndPointC: string = 'http://localhost:8080/product/mostrar';
private urlEndPointE: string = 'http://localhost:8080/product/editar';
private urlEndPointD: string = 'http://localhost:8080/product/eliminar';
private httpHeaders = new HttpHeaders({'Content-Type':'application/json'})

  constructor(private http: HttpClient, private router: Router) { }

  getProducts(): Observable<Producto[]> {
    //return of(productos);
    return this.http.get(this.urlEndPoint).pipe(
      map(response => response as Producto[])
    );
  }

  create(producto: Producto): Observable<Producto>{
    return this.http.post<Producto>(this.urlEndPointC,producto,{headers: this.httpHeaders}).pipe(
      catchError(e => {
        this.router.navigate(['/product']);
        console.error(e.error.mensaje);
        Swal.fire({
          type: 'error',
          title: 'Error de validacion',
          text: 'Verifique que los campos tengan los datos esperados y que el nombre no sea de un registro ya existente',
          footer: '<a href>Aque se debe esto?</a>'
        })
        return throwError(e);
      })
      );
  }
  getProduct(id): Observable<Producto>{
    return this.http.get<Producto>(`${this.urlEndPointC}/${id}`)
  }
  update(producto: Producto): Observable<Producto>{
    return this.http.put<Producto>(`${this.urlEndPointE}/${producto.id}`,producto,{headers: this.httpHeaders}).pipe(
    catchError(e => {
      console.error(e.error.mensaje);
      Swal.fire({
        type: 'error',
        title: 'Error',
        text: 'Error al tratar de actualizar el registro!',
        footer: '<a href>Aque se debe esto?</a>'
      })
      return throwError(e);
    })
    );
  }
  delete(id: number): Observable<Producto>{
    return this.http.delete<Producto>(`${this.urlEndPointD}/${id}`,{headers: this.httpHeaders}).pipe(
      catchError(e => {
        console.error(e.error.mensaje);
        alert(e.error.mensaje);
        Swal.fire({
          type: 'error',
          title: 'Error',
          text: 'Error al tratar de eliminar',
          footer: '<a href>Aque se debe esto?</a>'
        })
        return throwError(e);
      })
      );
  }
}

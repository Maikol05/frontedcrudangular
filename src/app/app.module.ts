import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { ProductoService } from './products/producto.service';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormComponent } from './products/form.component';
import { FormsModule } from '@angular/forms'

export const routes: Routes = [
  {path: '', redirectTo: '/products', pathMatch: 'full'},//no es necesario poner el slash que
  {path: 'products', component: ProductsComponent},
  {path: 'product/form', component: FormComponent},
  {path: 'product/form/:id', component: FormComponent}

];

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    FormComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ProductoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
